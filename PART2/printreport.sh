#!/bin/bash


if [ -z $1 ]
then
    echo "Usage: $0 <classlist>"
    exit
fi

if [ "$1" == "--help" ]
then
    echo -e "Usage: $0 <classlist>\n"
echo -e "<classlist> : Enter the class list, from which the printed report will be generated in CSV format.\n"
exit
fi

GRADE_FILE="$1_G"
REPORT_FILE="$1_report"

if [ ! -f "$GRADE_FILE" ]
then
echo "Sorry grade file not found."
echo "Exiting..."
exit
fi

if [ ! -f "$REPORT_FILE" ]
then
touch $REPORT_FILE
fi

FIRST_NAME[0]=""
LAST_NAME[0]=""
SID[0]=""
GRADE[0]=""
GRADE_LETTER[0]=""
s_count=0
f_count=0
l_count=0
g_count=0
gl_count=0

GRADE_COLUMN=0
GRADE_LETTER_COLUMN=0

IFS=":";
s_info=$(head -q -n 1 $GRADE_FILE)
s_array=($s_info);
for ((i=0; i<${#s_array[@]}; ++i));
do
    if [ "${s_array[$i]}" == "CURVE" ]
    then
    GRADE_COLUMN=$(($i+1))
    fi
done

IFS=":";
s_info=$(head -q -n 1 $GRADE_FILE)
s_array=($s_info);
for ((i=0; i<${#s_array[@]}; ++i));
do
    if [ "${s_array[$i]}" == "LETTER" ]
    then
    GRADE_LETTER_COLUMN=$(($i+1))
    fi
done


COLUMN_NO=$(awk -F: 'NR == 1 { print NF }' $GRADE_FILE)
ROW_COUNT=$(awk 'END{print FNR}' $GRADE_FILE)

for ((i=2; i<=$ROW_COUNT; i++))
do
for ((j=1; j<=$COLUMN_NO; j++))
do
    if [ $j -eq 1 ]
     then
     SID[$s_count]=$(awk -F ":" -v col=$j 'NR=="'"$i"'" {print $col}' OFS="\t" $GRADE_FILE)

     ((s_count++))
   fi

if [ $j -eq 2 ]
     then
     LAST_NAME[$l_count]=$(awk -F ":" -v col=$j 'NR=="'"$i"'" {print $col}' OFS="\t" $GRADE_FILE)

     ((l_count++))
   fi
if [ $j -eq 3 ]
     then
     FIRST_NAME[$f_count]=$(awk -F ":" -v col=$j 'NR=="'"$i"'" {print $col}' OFS="\t" $GRADE_FILE)

     ((f_count++))
   fi
if [ $j -eq $GRADE_COLUMN ]
     then
     GRADE[$g_count]=$(awk -F ":" -v col=$j 'NR=="'"$i"'" {print $col}' OFS="\t" $GRADE_FILE)

     ((g_count++))
   fi
if [ $j -eq $GRADE_LETTER_COLUMN ]
     then
     GRADE_LETTER[$gl_count]=$(awk -F ":" -v col=$j 'NR=="'"$i"'" {print $col}' OFS="\t" $GRADE_FILE)
     ((gl_count++))
   fi
done
done

echo "SAVING..."
IN=$GRADE_FILE
set -- "$IN" 
IFS="_"; declare -a Array=($*) 

echo -e "COURSE:${Array[0]}	SECTION:${Array[1]}	SEMSTER:FALL 200	NUMBER OF
STUDENTS:$(($ROW_COUNT-1))	CREDIT HOURS:3	PROF:ABDELSHAKOUR ABUZNEID\n" > "$REPORT_FILE"

echo -e "SID\tLAST_NAME\tFIRST_NAME\tGRADE\tGRADE_LETTER" >> "$REPORT_FILE"
echo -e "----\t----\t\t----\t\t----\t----" >> "$REPORT_FILE"

for ((i=0; i<=$(($ROW_COUNT-2)); i++))
do
echo -e "${SID[i]}\t${LAST_NAME[i]}\t\t${FIRST_NAME[i]}\t\t${GRADE[i]}\t${GRADE_LETTER[i]}" >> "$REPORT_FILE"
done
dat=$(date +'%d/%m/%Y')
echo -e "\n\nPROF SIGNATURE:					DATE:$dat" >> "$REPORT_FILE"

echo "SAVED... TO $REPORT_FILE"
