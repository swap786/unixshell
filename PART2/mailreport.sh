#!/bin/bash


if [[ -z $1 && -z $2 && -z $3 ]]
then
    echo -e "\nUsage: $0 <classlist> -registrar <registrar email_id>\n"
    exit
fi

if [ "$1" == "--help" ]
then
    echo -e "Usage: $0 <classlist> -registrar <registrar email_id>\n"
echo -e "<classlist> : Enter the class list from where, emails to curresponding students will be send.\n"
echo -e "-registrar : This flag indicates the email address of the registrar.\n"
echo -e "<registrar email_id> : This will be registrar email ID.\n"
exit
fi

REPORT_FILE="$1_report"
GRADE_FILE="$1_G"
CLASSLIST="$1"

if [ ! -f "$GRADE_FILE" ]
then
echo "Sorry grade file not found."
echo "Exiting..."
exit
fi

COLUMN_NO=$(awk -F: 'NR == 1 { print NF }' $GRADE_FILE)
ROW_COUNT=$(awk 'END{print FNR}' $GRADE_FILE)

GRADE[0]=""
GRADE_LETTER[0]=""
FIRST_NAME[0]=""
MAIL_LIST[0]=""
LAST_NAME[0]=""
f_count=1
l_count=1
g_count=1
gl_count=1
GRADE_COLUMN=0
GRADE_LETTER_COLUMN=0

IFS=":";
s_info=$(head -q -n 1 $GRADE_FILE)
s_array=($s_info);
for ((i=0; i<${#s_array[@]}; ++i));
do
    if [ "${s_array[$i]}" == "CURVE" ]
    then
    GRADE_COLUMN=$(($i+1))
    fi
done

IFS=":";
s_info=$(head -q -n 1 $GRADE_FILE)
s_array=($s_info);
for ((i=0; i<${#s_array[@]}; ++i));
do
    if [ "${s_array[$i]}" == "LETTER" ]
    then
    GRADE_LETTER_COLUMN=$(($i+1))
    fi
done


for ((i=2; i<=$ROW_COUNT; i++))
do
for ((j=1; j<=$COLUMN_NO; j++))
do

if [ $j -eq 2 ]
     then
     LAST_NAME[$l_count]=$(awk -F ":" -v col=$j 'NR=="'"$i"'" {print $col}' OFS="\t" $GRADE_FILE)

     ((l_count++))
   fi
if [ $j -eq 3 ]
     then
     FIRST_NAME[$f_count]=$(awk -F ":" -v col=$j 'NR=="'"$i"'" {print $col}' OFS="\t" $GRADE_FILE)

     ((f_count++))
   fi
if [ $j -eq $GRADE_COLUMN ]
     then
     GRADE[$g_count]=$(awk -F ":" -v col=$j 'NR=="'"$i"'" {print $col}' OFS="\t" $GRADE_FILE)

     ((g_count++))
   fi
if [ $j -eq $GRADE_LETTER_COLUMN ]
     then
     GRADE_LETTER[$gl_count]=$(awk -F ":" -v col=$j 'NR=="'"$i"'" {print $col}' OFS="\t" $GRADE_FILE)
     ((gl_count++))
   fi
done
done



for ((i=1; i<=$ROW_COUNT-1; i++))
do
    mail=$(awk -F ":" 'NR == "'"$i"'" {print $4}' $CLASSLIST)
   echo "$mail"
echo "${LAST_NAME[$i]}"
echo "${FIRST_NAME[$i]}"
echo "${GRADE[$i]}"
echo "${GRADE_LETTER[$i]}"

 curl -s --user 'api:key-7nc2d7e8uu7t13sezvor7yhcauaw-7t5'     https://api.mailgun.net/v3/sandbox94047.mailgun.org/messages     -F          from='AWK College Score <exavance@gmail.com>'     -F to="$mail"    -F subject='Semester Report'     -F text='This Is your Score of Final Semester!'     --form-string html="<html>Hello ${FIRST_NAME[$i]} ${LAST_NAME[$i]} You Got ${GRADE[$i]} Grades With Grade Letter ${GRADE_LETTER[$i]} .</html>"
done

echo -e "\nSending Grade Report to Registrar." 
 curl -s --user 'api:key-7nc2d7e8uu7t13sezvor7yhcauaw-7t5'     https://api.mailgun.net/v3/sandbox94047.mailgun.org/messages     -F          from='AWK College Report <exavance@gmail.com>'     -F to="$3"    -F subject='Semester Report'     -F text='Student Grade Report Sheet Sent By Proffesor.'     --form-string html='<html>Semester Report Sheet.</html>'   -F attachment=@"$REPORT_FILE"

echo -e "\nGrade Report Sent to Registrar." 
