#!/bin/bash

if [ -z $1 ]
then
    echo "Usage: $0 <classlist>"
    exit
fi

if [ "$1" == "--help" ]
then
    echo -e "Usage: $0 <classlist>\n"
echo -e "<classlist> : Enter the class list to which grades to be addedd, the added grades will be stored in the same file.\n"
exit
fi

COLUMN_NO=0
g_title=""
user_input=""
GRADE_FILE="$1_G"
#check if grade file already exist, if not create one.
if [ ! -f "$GRADE_FILE" ]
then
touch "$GRADE_FILE"
echo "SID:LNAME:FNAME" >> $GRADE_FILE
while IFS=':' read -u 3 a1 a2 a3 a4 a5
do 
echo "$a3:$a1:$a2" >> $GRADE_FILE
done 3< $1
fi
#ask for title of the grade
echo "WHAT IS THE TITLE OF THE GRADE?"
read g_title
#check for the columns of grade titles here.
IFS=":";
s_info=$(head -q -n 1 "$GRADE_FILE")
s_array=($s_info);
for ((i=0; i<${#s_array[@]}; ++i));
do
    if [ "${s_array[$i]}" == "$g_title" ]
    then
    COLUMN_NO=$(($i+1))
    break
    fi
done

if [ $COLUMN_NO -eq 0 ]
then
awk -F: 'NR == 1 {$(NF+1)="'"$g_title"'";}1' OFS=: "$GRADE_FILE" > tmpd && mv tmpd "$GRADE_FILE"
COLUMN_NO=$(awk -F: 'NR == 1 { print NF }' "$GRADE_FILE")
fi

echo "YOU ARE GOING TO ADD THE GRADE OF $g_title ....."
read response
#search for the student as per first,last,sid.
echo "HOW DO LIKE TO SEARCH FOR THE USER?((L/l)NAME/(F/f)NAME/(S/s)ID)?"
read user_opt

grade_count=0

while true;
do
if [[ "$user_opt" == "L" || "$user_opt" == "l" ]]
then
echo "LNAME:"
read user_input
if [ "$user_input" == "esc" ]
then
echo "YOU HAVE ADDED THE GRADE OF $g_title TO $grade_count STUDENTS OF $GRADE_FILE."
echo "QUIT NOW(Y/N)?y"
read input
if [ "$input" == "y" ]
then
echo "SORTING..."
awk 'NR == 1; NR > 1 {print $0 | "sort -n"}' $GRADE_FILE > tmpd && mv tmpd "$GRADE_FILE"
echo "SAVING"
exit
fi
fi
fi

if [[ "$user_opt" == "F" || "$user_opt" == "f" ]]
then
echo "FNAME:"
read user_input
if [ "$user_input" == "esc" ]
then
echo "YOU HAVE ADDED THE GRADE OF $g_title TO $grade_count STUDENTS OF $GRADE_FILE."
echo "QUIT NOW(Y/N)?y"
read input
if [ "$input" == "y" ]
then
echo "SORTING..."
awk 'NR == 1; NR > 1 {print $0 | "sort -n"}' $GRADE_FILE > tmpd && mv tmpd "$GRADE_FILE"
echo "SAVING"
exit
fi
fi
fi

if [[ "$user_opt" == "S" || "$user_opt" == "s" ]]
then
echo "SID:"
read user_input
if [ "$user_input" == "esc" ]
then
echo "YOU HAVE ADDED THE GRADE OF $g_title TO $grade_count STUDENTS OF $GRADE_FILE."
echo "QUIT NOW(Y/N)?y"
read input
if [ "$input" == "y" ]
then
echo "SORTING..."
awk 'NR == 1; NR > 1 {print $0 | "sort -n"}' $GRADE_FILE > tmpd && mv tmpd "$GRADE_FILE"
echo "SAVING"
exit
fi
fi
fi

count=1
i=1
while IFS=':' read -u 3 a1 a2 a3 a4
do 
test $i -eq 1 && ((i++)) && continue
((count++))

if [[ "$user_opt" == "S" || "$user_opt" == "s" ]]
then
if [ "$a1" == "$user_input" ]
then
echo "YOU ARE GOING TO USE THE SID. PLEASE MAKE SURE THAT THE NAME IS MATCHING ......"
echo "IT'S $a2, $a3. CORRECT(Y/N)?"
read user_input
if [ "Y" == "$user_input" ]
then
echo "THE GRADE OF $g_title: "
read grade
((grade_count++))
awk -F: 'NR == "'"$count"'" {$(0+"'"$COLUMN_NO"'")="'"$grade"'";}1' OFS=: "$GRADE_FILE" > tmpd && mv tmpd "$GRADE_FILE"
else
echo "SKIP TO THE NEXT USER OR SEARCH AGAIN?((L)ASTNAME/(F)NAME/(S)KIP?l"
read input

fi
fi
fi

if [[ "$user_opt" == "L" || "$user_opt" == "l" ]]
then
if [ "$a2" == "$user_input" ]
then
echo "YOU ARE GOING TO USE THE LNAME. PLEASE MAKE SURE THAT THE NAME IS MATCHING ......"
echo "IT'S $a2, $a3. CORRECT(Y/N)?"
read user_input
if [ "Y" == "$user_input" ]
then
echo "THE GRADE OF $g_title: "
read grade
((grade_count++))
awk -F: 'NR == "'"$count"'" {$(0+"'"$COLUMN_NO"'")="'"$grade"'";}1' OFS=: "$GRADE_FILE" > tmpd && mv tmpd "$GRADE_FILE"
fi
fi
fi

if [[ "$user_opt" == "F" || "$user_opt" == "f" ]]
then
if [ "$a3" == "$user_input" ]
then
echo "YOU ARE GOING TO USE THE FNAME. PLEASE MAKE SURE THAT THE NAME IS MATCHING ......"
echo "IT'S $a2, $a3. CORRECT(Y/N)?"
read user_input
if [ "Y" == "$user_input" ]
then
echo "THE GRADE OF $g_title: "
read grade
((grade_count++))
awk -F: 'NR == "'"$count"'" {$(0+"'"$COLUMN_NO"'")="'"$grade"'";}1' OFS=: "$GRADE_FILE" > tmpd && mv tmpd "$GRADE_FILE"
fi
fi
fi

done 3< $GRADE_FILE

done
