#!/bin/bash

if [ -z $1 ]
then
    echo "Usage: $0 <classlist>"
    exit
fi

if [ "$1" == "--help" ]
then
    echo -e "Usage: $0 <classlist>\n"
echo -e "<classlist> : Enter the class list from which grade report will be generated, grades will be stored in the same file.\n"
exit
fi



GRADE_FILE="$1_G"
CAL_GRADE_FILE="$1_G_TOTAL"
#check whether grade file exist or not.
if [ ! -f "$GRADE_FILE" ]
then
echo "Sorry grade file not found."
echo "Exiting..."
exit
fi
#initialize variables needed to claculate grades, of each student
FLAG=0
POLICY_COUNTER=0
GRADE_POLICY[0]=0
LETTER_GRADE_A[0]=0
LETTER_GRADE_AM[0]=0
LETTER_GRADE_BP[0]=0
LETTER_GRADE_B[0]=0
LETTER_GRADE_BM[0]=0
LETTER_GRADE_CP[0]=0
LETTER_GRADE_C[0]=0
LETTER_GRADE_CM[0]=0
LETTER_GRADE_DP[0]=0
LETTER_GRADE_D[0]=0
LETTER_GRADE_DM[0]=0
LETTER_GRADE_F[0]=0

echo "ENTER $1 POLICY,"
#enter the grade policy here.
IFS=":";
s_info=$(head -q -n 1 "$GRADE_FILE")
s_array=($s_info);
for ((i=0; i<${#s_array[@]}; ++i));
do
    if [[ "${s_array[$i]}" == "GRADE" || "${s_array[$i]}" == "CURVE" || "${s_array[$i]}" == "LETTER" ]]
    then
    ((FLAG++))
    fi
done

if [ $FLAG -eq 0 ]
then
awk -F: 'NR == 1 {$(NF+1)="GRADE";}1' OFS=: "$GRADE_FILE" > tmpd && mv tmpd "$GRADE_FILE"
awk -F: 'NR == 1 {$(NF+1)="CURVE";}1' OFS=: "$GRADE_FILE" > tmpd && mv tmpd "$GRADE_FILE"
awk -F: 'NR == 1 {$(NF+1)="LETTER";}1' OFS=: "$GRADE_FILE" > tmpd && mv tmpd "$GRADE_FILE"
fi

COLUMN_NO=$(awk -F: 'NR == 1 { print NF }' $GRADE_FILE)
CAL_COLUMN_NO=$(awk -F: 'NR == 1 { print NF }' $CAL_GRADE_FILE)
ROW_COUNT=$(awk 'END{print FNR}' $GRADE_FILE)

COLUMN_NO=$(($COLUMN_NO-3))

for ((i=4; i<=$CAL_COLUMN_NO; i++))
do
    var=$(awk -F ":" -v col=$i 'NR==1 {print $col}' OFS=: $CAL_GRADE_FILE) 
    echo "POLICY FOR $var....."
    read policy
    GRADE_POLICY[POLICY_COUNTER]=$policy   
    ((POLICY_COUNTER++))
done

IFS=" "
  echo "A......."
    read input
    LETTER_GRADE_A=($input)
    echo "A-......."
    read input
    LETTER_GRADE_AM=($input)
    echo "B+......."
    read input
    LETTER_GRADE_BP=($input)
    echo "B......."
    read input
    LETTER_GRADE_B=($input) 
    echo "B-......."
    read input
    LETTER_GRADE_BM=($input)
    echo "C+......."
    read input
    LETTER_GRADE_CP=($input)
    echo "C......."
    read input
    LETTER_GRADE_C=($input)
    echo "C-......."
    read input
    LETTER_GRADE_CM=($input)
    echo "D+......."
    read input
    LETTER_GRADE_DP=($input)
    echo "D......."
    read input
    LETTER_GRADE_D=($input)
    echo "D-......."
    read input
    LETTER_GRADE_DM=($input)
    echo "F......."
    read input
    LETTER_GRADE_F=($input)

curve_point=0

echo "DO YOU LIKE TO CURVE(Y/N)?"
read curve

if [ "$curve" == 'Y' ]
then
	echo "HOW DO YOU LIKE TO CURVE?(1/2)"
	echo "1. ADD POINTS"
	echo "2. ADD (100 - HIGHEST GRADE) POINTS TO ALL STUDENTS"
	echo "ENTER YOUR OPTION:"
	read option
	if [ "$option" == '1' ]
	then
	echo "HOW MANY POINTS:"
	read curve_point
	
	fi	
	if [ "$option" == '2' ]
	then	
	curve_point=100
	fi	
	
fi

for ((i=2; i<=$ROW_COUNT; i++))
do
PERCENTAGE=0
partial_total=0
Total=0
new_grade_index=0
grade_index=4
RISE_PERCENTAGE=0
letter=""
for ((j=4; j<=$CAL_COLUMN_NO; j++))
do

     abc=$(awk -F ":" -v col=$j 'NR=="'"$i"'" {print $col}' $CAL_GRADE_FILE)
     if [ "$abc" == "" ]
     then
          abc=0
     fi
     new_grade_index=$(($j - $grade_index)) 	
     partial_total=$(($abc * ${GRADE_POLICY[$new_grade_index]}))
     #echo "Partial $partial_total"
     Total=$(($Total + $partial_total))	
     #echo "total $Total"
done
PERCENTAGE=$(expr $Total*0.01 |bc)
echo "$PERCENTAGE"
	RISE_PERCENTAGE=$(expr $curve_point+$PERCENTAGE |bc)
if [[ $(echo "$RISE_PERCENTAGE >= ${LETTER_GRADE_A[0]}"|bc) -eq 1 && $(echo "$RISE_PERCENTAGE <= ${LETTER_GRADE_A[1]}"|bc) -eq 1 ]]
     then
     letter="A"
elif [[ $(echo "$RISE_PERCENTAGE >= ${LETTER_GRADE_AM[0]}"|bc) -eq 1 && $(echo "$RISE_PERCENTAGE <= ${LETTER_GRADE_AM[1]}"|bc) -eq 1 ]]
then
	letter="A-"
elif [[ $(echo "$RISE_PERCENTAGE >= ${LETTER_GRADE_BP[0]}"|bc) -eq 1 && $(echo "$RISE_PERCENTAGE <= ${LETTER_GRADE_BP[1]}"|bc) -eq 1 ]]
then
	letter="B+"

elif [[ $(echo "$RISE_PERCENTAGE >= ${LETTER_GRADE_B[0]}"|bc) -eq 1 && $(echo "$RISE_PERCENTAGE <= ${LETTER_GRADE_B[1]}"|bc) -eq 1 ]]
then
	letter="B"

elif [[ $(echo "$RISE_PERCENTAGE >= ${LETTER_GRADE_BM[0]}"|bc) -eq 1 && $(echo "$RISE_PERCENTAGE <= ${LETTER_GRADE_BM[1]}"|bc) -eq 1 ]]
then
	letter="B-"

elif [[ $(echo "$RISE_PERCENTAGE >= ${LETTER_GRADE_CP[0]}"|bc) -eq 1 && $(echo "$RISE_PERCENTAGE <= ${LETTER_GRADE_CP[1]}"|bc) -eq 1 ]]
then
	letter="C+"

elif [[ $(echo "$RISE_PERCENTAGE >= ${LETTER_GRADE_C[0]}"|bc) -eq 1 && $(echo "$RISE_PERCENTAGE <= ${LETTER_GRADE_C[1]}"|bc) -eq 1 ]]
then
	letter="C"

elif [[ $(echo "$RISE_PERCENTAGE >= ${LETTER_GRADE_CM[0]}"|bc) -eq 1 && $(echo "$RISE_PERCENTAGE <= ${LETTER_GRADE_CM[1]}"|bc) -eq 1 ]]
then
	letter="C-"

elif [[ $(echo "$RISE_PERCENTAGE >= ${LETTER_GRADE_DP[0]}"|bc) -eq 1 && $(echo "$RISE_PERCENTAGE <= ${LETTER_GRADE_DP[1]}"|bc) -eq 1 ]]
then
	letter="D+"

elif [[ $(echo "$RISE_PERCENTAGE >= ${LETTER_GRADE_D[0]}"|bc) -eq 1 && $(echo "$RISE_PERCENTAGE <= ${LETTER_GRADE_D[1]}"|bc) -eq 1 ]]
then
	letter="D"

elif [[ $(echo "$RISE_PERCENTAGE >= ${LETTER_GRADE_DM[0]}"|bc) -eq 1 && $(echo "$RISE_PERCENTAGE <= ${LETTER_GRADE_DM[1]}"|bc) -eq 1 ]]
then
	letter="D-"

elif [[ $(echo "$RISE_PERCENTAGE >= ${LETTER_GRADE_F[0]}"|bc) -eq 1 && $(echo "$RISE_PERCENTAGE <= ${LETTER_GRADE_F[1]}"|bc) -eq 1 ]]
then
	letter="F"
fi

awk -F: 'NR == "'"$i"'" {$("'"$COLUMN_NO"'"+1)="'"$PERCENTAGE"'";}1' OFS=: "$GRADE_FILE" > tmpd && mv tmpd "$GRADE_FILE"
awk -F: 'NR == "'"$i"'" {$("'"$COLUMN_NO"'"+2)="'"$RISE_PERCENTAGE"'";}1' OFS=: "$GRADE_FILE" > tmpd && mv tmpd "$GRADE_FILE"
awk -F: 'NR == "'"$i"'" {$("'"$COLUMN_NO"'"+3)="'"$letter"'";}1' OFS=: "$GRADE_FILE" > tmpd && mv tmpd "$GRADE_FILE"
done


