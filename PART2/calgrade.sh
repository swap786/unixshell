#!/bin/bash

if [ -z $1 ]
then
    echo "Usage: $0 <classlist>"
    exit
fi

if [ "$1" == "--help" ]
then
    echo -e "Usage: $0 <classlist>\n"
echo -e "<classlist> : Enter the class list to which grades to be calculated, the sum of grades will be stored in classlist_G_TOTAL.\n"
exit
fi

CAL_GRADE_FILE="$1_G_TOTAL"
GRADE_FILE="$1_G"
HW[0]=0
EXAM[0]=0
QUIZ[0]=0
PROJ[0]=0
HWCNT=1
EXAMCNT=1
QUIZCNT=1
PROJCNT=1
HWTOTAL=0
EXAMTOTAL=0
QUIZTOTAL=0
PROJTOTAL=0
HWCOL=0
EXAMCOL=0
QUIZCOL=0
PROJCOL=0

if [ ! -f "$CAL_GRADE_FILE" ]
then
touch "$CAL_GRADE_FILE"
echo "SID:LNAME:FNAME" >> $CAL_GRADE_FILE
while IFS=':' read -u 3 a1 a2 a3 a4 a5
do 
echo "$a3:$a1:$a2" >> $CAL_GRADE_FILE
done 3< $1
fi
awk 'NR == 1; NR > 1 {print $0 | "sort -n"}' $CAL_GRADE_FILE > tmpd && mv tmpd "$CAL_GRADE_FILE"
c_name=""
g_total=""

COLUMN_NO=$(awk -F: 'NR == 1 { print NF }' $GRADE_FILE)
 
for ((i=4; i<=$COLUMN_NO; i++))
do
    c_name=$(awk -F ":" -v col=$i 'NR==1 {print $col}' OFS=: $GRADE_FILE)

    title="$(echo "$c_name" | grep -i hw)"
    if printf -- '%s' "$(echo "$c_name" | grep -i hw)" | egrep -q -- "hw"
    then
    HW[$HWCNT]=$i
    ((HWCNT++))
    fi

    title="$(echo "$c_name" | grep -i exam)"
    if printf -- '%s' "$(echo "$c_name" | grep -i exam)" | egrep -q -- "exam"
    then
    EXAM[$EXAMCNT]=$i
    ((EXAMCNT++))
    fi

    title="$(echo "$c_name" | grep -i quiz)"
    if printf -- '%s' "$(echo "$c_name" | grep -i quiz)" | egrep -q -- "quiz"
    then
    QUIZ[$QUIZCNT]=$i
    ((QUIZCNT++))

    fi
 
    title="$(echo "$c_name" | grep -i proj)"
    if printf -- '%s' "$(echo "$c_name" | grep -i proj)" | egrep -q -- "proj"
    then
    PROJ[$PROJCNT]=$i
    ((PROJCNT++))
    
    fi
done

if [ ${#QUIZ[@]} -gt 1 ]; then
    awk -F: 'NR == 1 {$(NF+1)="QUIZ";}1' OFS=: "$CAL_GRADE_FILE" > tmpd && mv tmpd "$CAL_GRADE_FILE"
    QUIZCOL=$(awk -F: 'NR == 1 { print NF }' $CAL_GRADE_FILE)

fi

if [ ${#HW[@]} -gt 1 ]; then
    awk -F: 'NR == 1 {$(NF+1)="HOMEWORK";}1' OFS=: "$CAL_GRADE_FILE" > tmpd && mv tmpd "$CAL_GRADE_FILE"
    HWCOL=$(awk -F: 'NR == 1 { print NF }' $CAL_GRADE_FILE)
fi

if [ ${#EXAM[@]} -gt 1 ]; then
    awk -F: 'NR == 1 {$(NF+1)="EXAM";}1' OFS=: "$CAL_GRADE_FILE" > tmpd && mv tmpd "$CAL_GRADE_FILE"
    EXAMCOL=$(awk -F: 'NR == 1 { print NF }' $CAL_GRADE_FILE)
fi

if [ ${#PROJ[@]} -gt 1 ]; then
    awk -F: 'NR == 1 {$(NF+1)="PROJECT";}1' OFS=: "$CAL_GRADE_FILE" > tmpd && mv tmpd "$CAL_GRADE_FILE"
    PROJCOL=$(awk -F: 'NR == 1 { print NF }' $CAL_GRADE_FILE)
fi


ROW_COUNT=$(awk 'END{print FNR}' $GRADE_FILE)


if [ ${#HW[@]} -gt 1 ]; then
for ((i=2; i<=$ROW_COUNT; i++))
do
HWTOTAL=0
total=0
    for ((j=1; j<${#HW[@]}; ++j))
    do
    HWTOTAL=$(awk -F: 'NR == "'"$i"'" {print $(0+"'"${HW[$j]}"'");}' OFS=: "$GRADE_FILE")
	
     if [ "$HWTOTAL" == "" ]
      then
	HWTOTAL=0	
	fi
	total=$(($HWTOTAL+$total))
    done
awk -F: 'NR == "'"$i"'" {$("'"$HWCOL"'"+0)="'"$total"'";}1' OFS=: "$CAL_GRADE_FILE" > tmpd && mv tmpd "$CAL_GRADE_FILE"
done
echo "ADDING HW GRADES FOR ALL STUDENTS ...... DONE"
fi
   
if [ ${#EXAM[@]} -gt 1 ]; then
for ((i=2; i<=$ROW_COUNT; i++))
do
EXAMTOTAL=0
total=0
    for ((j=1; j<${#EXAM[@]}; ++j))
    do
    EXAMTOTAL=$(awk -F: 'NR == "'"$i"'" {print $(0+"'"${EXAM[$j]}"'");}' OFS=: "$GRADE_FILE")
	
     if [ "$EXAMTOTAL" == "" ]
      then
	EXAMTOTAL=0	
	fi
	total=$(($EXAMTOTAL+$total))
    done

awk -F: 'NR == "'"$i"'" {$("'"$EXAMCOL"'"+0)="'"$total"'";}1' OFS=: "$CAL_GRADE_FILE" > tmpd && mv tmpd "$CAL_GRADE_FILE"
done
echo "ADDING EXAM GRADES FOR ALL STUDENTS ...... DONE"
fi

if [ ${#QUIZ[@]} -gt 1 ]; then
for ((i=2; i<=$ROW_COUNT; i++))
do
QUIZTOTAL=0
total=0
    for ((j=1; j<${#QUIZ[@]}; ++j))
    do
    QUIZTOTAL=$(awk -F: 'NR == "'"$i"'" {print $(0+"'"${QUIZ[$j]}"'");}' OFS=: "$GRADE_FILE")
	
     if [ "$QUIZTOTAL" == "" ]
      then
	QUIZTOTAL=0	
	fi
	total=$(($QUIZTOTAL+$total))
    done

awk -F: 'NR == "'"$i"'" {$("'"$QUIZCOL"'"+0)="'"$total"'";}1' OFS=: "$CAL_GRADE_FILE" > tmpd && mv tmpd "$CAL_GRADE_FILE"
done
echo "ADDING QUIZ GRADES FOR ALL STUDENTS ...... DONE"
fi

if [ ${#PROJ[@]} -gt 1 ]; then
for ((i=2; i<=$ROW_COUNT; i++))
do
PROJTOTAL=0
total=0
    for ((j=1; j<${#PROJ[@]}; ++j))
    do
    PROJTOTAL=$(awk -F: 'NR == "'"$i"'" {print $(0+"'"${PROJ[$j]}"'");}' OFS=: "$GRADE_FILE")
	
     if [ "$PROJTOTAL" == "" ]
      then
	PROJTOTAL=0	
	fi
	total=$(($PROJTOTAL+$total))
    done

awk -F: 'NR == "'"$i"'" {$("'"$PROJCOL"'"+0)="'"$total"'";}1' OFS=: "$CAL_GRADE_FILE" > tmpd && mv tmpd "$CAL_GRADE_FILE"
done
echo "ADDING PROJ GRADES FOR ALL STUDENTS ...... DONE"
fi



