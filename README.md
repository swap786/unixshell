=========================PART1==================================
1. to add new student: 
	./add.sh -1l ==> This will ask only one time to enter colon seperated record for student.
		Ex: ./add.sh -1l
	./add.sh -1l XX ==> This will ask XX times to enter colon seperated record for student. 
		Ex: ./add.sh -1l 5
	./add.sh -il ==> This will ask only one time to enter student record, but it will ask interactively, like FNAME, LNAME, SID, etc.
		Ex: ./add.sh -il
	./add.sh -il XX ==> This will ask 'XX' times to enter student record, but it will ask interactively, like FNAME, LNAME, SID, etc.
		Ex: ./add.sh -il 5

2. To modify existing student record:
	./modify.sh [-email|-lname|-fname|-sid|-phone] classlist_name last_name first_name
		Here in order to modify email of student, then
			./modify.sh -email classlist_name lastname firstname
		sae can be done in order to modify other parameters like LNAME,FNAME,SID,etc.

3. To mail assignments to students by proffesor:
	./email.sh assignment_file classlist-1 classlist-2 ...classlist-N
		Here supoose you want to send assignment cse123.doc to all students in classlist cse23 and cse24 then:
			./email.sh cse123.doc cse23 cse24
			[NOTE: assignment should be full path to file name, if not in current directory.]

4. To remove classlist do:
	./remove.sh classlist_name
		Ex: ./remove cse23

5. To rename classlist do:
	./rename.sh classlist_name to_new_classlist_name
		Ex: ./rename.sh cse23 cse23_back

6. To remove perticular student from classlist:
	./rmstud.sh classlist_name last_name first_name
		Ex: ./rmstud.sh cse23 Udapure Swapnil ==> this will remove student with last name 'Udapure' & first name 'Swapnil'

=========================PART2==================================

7. To add grades to students in the class list:
	./addgrade.sh classlist
		Ex: ./addgrade.sh cse23 ==> this will add grades to students for specific titles like, hw01,proj01,etc, and will store it file named cse_G in same directory.

8. To calculate grades for all students in the classlist for specific titles do:
	./calgrade.sh classlist
		Ex: ./calgrade.sh cse23 ==> this will calculate(sum) all grades for all titles in the classlist and save it to cse23_G_TOTAL

9. To calculate grades for each student in the classlist
	./gradereport.sh classlist
		Ex: ./gradereport.sh cse23 ==> This will calculate grades for each student, and save it in cse_G file, where it will then add three columns, GRADE, CURVE, LETTER, which indicates grades for each student in the classlist.

10. To print report in tabular CSV format do:
	./printreport.sh classlist
		Ex: ./printreport.sh cse23_09 ==> This will print report in tabular CSV format and save to cse23_09_report file.

11. To mail grades of each student to there mail address, also mail all grade report files to registrar do:
	./mailreport.sh classlist -registrar registrar_email_id
		Ex: ./mailreport.sh cse23_09 -registrar registrar@gmail.com ==> this will mail students grades from the classlist, also mail the grade report to registrar.

