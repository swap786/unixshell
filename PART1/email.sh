#!/bin/bash

if [ -z $1 ]
then
    echo "Usage: $0 <assignment_attachment> <classlist-1 classlist-2 ...classlist-N>"
    exit
fi

if [ "$1" == "--help" ]
then
    echo -e "Usage: $0 <assignment_attachment> <classlist-1 classlist-2 ...classlist-N>\n"
echo -e "<assignment_attachment> : Enter the assignment(File) that to be attached.\n"
echo -e "<classlist-1 classlist-2 ...classlist-N> : space seperated class lists, whose students will recieve assignement attachment.\n"
exit
fi

IFS=$'\n'       # make newlines the only separator
set -f          # disable globbing

for var in "${@:2}"; do
echo $var
for j in $(cat "$var"); do
IFS=":";
#get the list of emails of all students.
s_array=($j);
#loop through all email ID's
for ((i=0; i<${#s_array[@]}; ++i));
do
    if [ $i -eq 3 ]
    then
    to="${s_array[$i]}"
    echo "Sending To :$to"
   #command to send email to all mail list, using mailgun mailing service, using HTTP POST, method.
    curl -s --user 'api:key-7nc2d7e8uu7t13sezvor7yhcauaw-7t5'     https://api.mailgun.net/v3/sandbox94047.mailgun.org/messages     -F          from='Excited User <swapnil.udapure5@gmail.com>'     -F to="$to"    -F subject='College Assignment'     -F text='Assignment Send By Proffesor!'     --form-string html='<html>Check Out this Assignment.</html>'   -F attachment=@"$1"
    
    fi
done

IFS=$'\n'
done

done

