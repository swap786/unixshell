#!/bin/bash

#if argument does't match will show up the usage.
if [ -z $1 ]
then
    echo "Usage: $0 <classlist name> -1l|[-1l -XX] OR -il|[-il -XX]"
    exit
fi

#show help info, about the command.
if [ "$1" == "--help" ]
then
echo "Usage: $0 <classlist name> -1l|[-1l -XX] OR -il|[-il -XX]"
echo -e "<classlist name> : This is Name of classlist to which student information will be added.\n"
echo -e "-1l|[-1l -XX] : Specify either of option, for -1l it will ask only one time and you have to add colon seperated record.\n"
echo -e "[-1l -XX] : if specified like -1l 5 it will ask five times and you have to add colon seperated record for 5(i.e 'N') students.\n"
echo -e "-il|[-il -XX] : Specify either of option, for -il it will ask only one time and it will start interactive interface to get information anout each student.\n"
echo -e "[-il -XX] : if specified like -il 5 it will ask five times and you have to add student record in interactive manner.\n"
exit
fi
#check whether it is interactive shell.
if [[ "$2" == "-il" && "$3" == "" ]]
then
echo $1
echo "Enter SID: "
read sid

IFS=$'\n'       # make newlines the only separator
set -f          # disable globbing
COUNT=":$sid:"
#check if file doesn't exist, if not then create one.
if [ ! -f $1 ]
then
touch $1
fi
#loop through all records in classlist.
for i in $(cat "$1"); do
line+=$(echo $i|awk '/'$COUNT'/{print $0}')$'\n'
done
line="${line#"${line%%[![:space:]]*}"}"   # remove leading whitespace characters
line="${line%"${line##*[![:space:]]}"}"   # remove trailing whitespace characters

if [ "$line" == "" ]
then
echo "Enter Last Name: "
read lnm
echo "Enter First Name: "
read fnm
echo "Enter Email: "
read email
echo "Enter Phone: "
read phone
#redirect the info taken to classlist
echo "$lnm:$fnm:$sid:$email:$phone">>$1
#sort by last name.
sort -o $1 -k 1 $1

else
#check whether user already exist.
var=$(echo $line | sed s/':'/' '/g)
echo "The User already have this key: $var"
echo "ADD THIS STUDENT(Y/N)?"
read sel
if [ "$sel" == "Y" ]
then
echo "Enter Last Name: "
read lnm
echo "Enter First Name: "
read fnm
echo "Enter Email: "
read email
echo "Enter Phone: "
read phone
echo "$lnm:$fnm:$sid:$email:$phone">>$1
sort -o $1 -k 1 $1
fi
fi
fi


###############
#check for the interactive shell.
if [[ "$2" == "-il" && "$3" != "" ]]
then
#itterrate N times inputed as per third argument.
for p in $(seq 1 $3)
do
line=""
echo "Enter SID: "
read sid

IFS=$'\n'       # make newlines the only separator
set -f          # disable globbing
COUNT=":$sid:"
if [ ! -f $1 ]
then
touch $1
fi
for i in $(cat "$1"); do
line+=$(echo $i|awk '/'$COUNT'/{print $0}')$'\n'
done
line="${line#"${line%%[![:space:]]*}"}"   # remove leading whitespace characters
line="${line%"${line##*[![:space:]]}"}"   # remove trailing whitespace characters
#the steps below are repeated as per the different option from command line.
if [ "$line" == "" ]
then
echo "Enter Last Name: "
read lnm
echo "Enter First Name: "
read fnm
echo "Enter Email: "
read email
echo "Enter Phone: "
read phone
echo "$lnm:$fnm:$sid:$email:$phone">>$1
sort -o $1 -k 1 $1

else
var=$(echo $line | sed s/':'/' '/g)
echo "The User already have this key: $var"
echo "ADD THIS STUDENT(Y/N)?"
read sel
if [ "$sel" == "Y" ]
then
echo "Enter Last Name: "
read lnm
echo "Enter First Name: "
read fnm
echo "Enter Email: "
read email
echo "Enter Phone: "
read phone
echo "$lnm:$fnm:$sid:$email:$phone">>$1
sort -o $1 -k 1 $1
fi
fi
done
fi

################################



if [[ "$2" == "-1l" && "$3" == "" ]]
then

sid=""
line=""
echo "Enter Info: "
read s_info

IFS=":";

s_array=($s_info);
for ((i=0; i<${#s_array[@]}; ++i));
do
    if [ $i -eq 2 ]
    then
    sid="${s_array[$i]}"
    break
    fi
done


IFS=$'\n'       # make newlines the only separator
set -f          # disable globbing
COUNT=":$sid:"
if [ ! -f $1 ]
then
touch $1
fi
for i in $(cat "$1"); do
line+=$(echo $i|awk '/'$COUNT'/{print $0}')$'\n'
done
line="${line#"${line%%[![:space:]]*}"}"   # remove leading whitespace characters
line="${line%"${line##*[![:space:]]}"}"   # remove trailing whitespace characters

if [ "$line" == "" ]
then
echo "$s_info">>$1
sort -o $1 -k 1 $1

else
var=$(echo $line | sed s/':'/' '/g)
echo "The User already have this key: $var"
echo "ADD THIS STUDENT(Y/N)?"
read sel
if [ "$sel" == "Y" ]
then
echo "$s_info">>$1
sort -o $1 -k 1 $1
fi
fi
fi



##################################

if [[ "$2" == "-1l" && "$3" != "" ]]
then

for p in $(seq 1 $3)
do
sid=""
line=""
echo "Enter Student $p: "
read s_info

IFS=":";

s_array=($s_info);
for ((i=0; i<${#s_array[@]}; ++i));
do
    if [ $i -eq 2 ]
    then
    sid="${s_array[$i]}"
    break
    fi
done


IFS=$'\n'       # make newlines the only separator
set -f          # disable globbing
COUNT=":$sid:"
if [ ! -f $1 ]
then
touch $1
fi
for i in $(cat "$1"); do
line+=$(echo $i|awk '/'$COUNT'/{print $0}')$'\n'
done
line="${line#"${line%%[![:space:]]*}"}"   # remove leading whitespace characters
line="${line%"${line##*[![:space:]]}"}"   # remove trailing whitespace characters

if [ "$line" == "" ]
then
echo "$s_info">>$1
sort -o $1 -k 1 $1

else
var=$(echo $line | sed s/':'/' '/g)
echo "The User already have this key: $var"
echo "ADD THIS STUDENT(Y/N)?"
read sel
if [ "$sel" == "Y" ]
then
echo "$s_info">>$1
sort -o $1 -k 1 $1
fi
fi
done
fi
