#!/bin/bash

if [ -z $1 ]
then
    echo "Usage: $0 <classlist_name> <to_new_classlist_name>"
    exit
fi

if [ "$1" == "--help" ]
then
    echo -e "Usage: $0 <classlist_name> <to_new_classlist_name>\n"
echo -e "<classlist_name> : Enter the class list name to be rename.\n"
echo -e "<to_new_classlist_name> : Enter the class list name to rename to.\n"
exit
fi

if [[ "$1" != "" && "$2" != "" ]]
then
mv -v "$1" "$2"
else
echo "Invalid Input."
fi
