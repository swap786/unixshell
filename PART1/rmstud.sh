#!/bin/bash

if [ -z $1 ]
then
    echo "Usage: $0 <classlist name> <last_name & first_name>"
    exit
fi

if [ "$1" == "--help" ]
then
    echo -e "Usage: $0 <classlist name> <last_name & first_name>\n"
echo -e "<classlist_name> : Enter the class list name from where students will be deleted.\n"
echo -e "<last_name & first_name> : This flag specifies, student record would be searched by it's First Name and Last Name. \n"
exit
fi

FLAG=0
IFS=$'\n'       # make newlines the only separator
set -f          # disable globbing
#loop through all records in the file.
for j in $(cat "$1"); do
patt="$2:$3"
line=$(echo $j|awk '/'$patt'/{print $0}')
if [[ "$2" != "" && "$3" != "" ]]
then
if [ "$line" != "" ]
then
#remove the student from the file, which matches the given criteria.
sed -i /"$patt"/d $1
echo "$line Removed."
FLAG=1
break
fi
else
FLAG=2
break
fi
done

if [ $FLAG == 0 ]
then
echo "Name doesn't match."
elif [ $FLAG == 2 ]
then
echo "null"
fi

