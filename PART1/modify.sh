#!/bin/bash

#if argument does't match will show up the usage.
if [ -z $1 ]
then
    echo "Usage: $0 [-email|-lname|-fname|-sid|-phone] <classlist name> <last_name & first_name>"
    exit
fi

#show help info, about the command.
if [ "$1" == "--help" ]
then
echo -e "Usage: $0 [-email|-lname|-fname|-sid|-phone] <classlist name> <last_name & first_name>\n"
echo -e "[-email|-lname|-fname|-sid|-phone] : This is the feild that you want to modify from student classlist.\n"
echo -e "<classlist name> : This is Name of classlist where student information is stored.\n"
echo -e "<last_name & first_name> : This flag specifies, student record would be searched by it's First Name and Last Name. \n"

exit
fi

IFS=$'\n'       # make newlines the only separator
set -f          # disable globbing
#read the number of lines from classlist.
for j in $(cat "$2"); do
patt="$3:$4"
#get the pattern for matching fname & lname.
line=$(echo $j|awk '/'$patt'/{print $0}')
old_line=$line
if [[ "$3" != "" && "$4" != "" ]]
then
read_data=""
old_data=""

IFS=":";
#read the contents of the file stored in the following array.
s_array=($old_line);
for ((i=0; i<${#s_array[@]}; ++i));
do
#here change the email.
    if [ "$1" == "-email" ]
    then
    if [ $i -eq 3 ]
    then
    old_data="${s_array[$i]}"
    #if email is found for the student then ask to modify the parameters.
    echo "CURRENT EMAIL ADDRESS IS :$old_data"
    echo "ENTER NEW EMAIL ADDRESS :"
    read read_data
    var=$(echo "$old_line" | sed s/$old_data/$read_data/g)
    echo "OLD: $old_line"
    echo "NEW: $var"
    sed -i s/"$old_line"/"$var"/g $2
    break
    fi
    fi

    if [ "$1" == "-fname" ]
    then
    if [ $i -eq 1 ]
    then
    old_data="${s_array[$i]}"
    echo "CURRENT FIRST NAME IS :$old_data"
    echo "ENTER NEW FIRST NAME :"
    read read_data
    var=$(echo "$old_line" | sed s/$old_data/$read_data/g)
    echo "OLD: $old_line"
    echo "NEW: $var"
    sed -i s/"$old_line"/"$var"/g $2
    break
    fi
    fi
#here modify the last name.
    if [ "$1" == "-lname" ]
    then
    if [ $i -eq 0 ]
    then
    old_data="${s_array[$i]}"
    echo "CURRENT LAST NAME IS :$old_data"
    echo "ENTER NEW LAST NAME :"
    read read_data
    var=$(echo "$old_line" | sed s/$old_data/$read_data/g)
    echo "OLD: $old_line"
    echo "NEW: $var"
    sed -i s/"$old_line"/"$var"/g $2
    break
    fi
    fi
    #modify the phone number.
    if [ "$1" == "-phone" ]
    then
    if [ $i -eq 4 ]
    then
    old_data="${s_array[$i]}"
    echo "CURRENT PHONE NO. IS :$old_data"
    echo "ENTER NEW PHONE NO. :"
    read read_data
    var=$(echo "$old_line" | sed s/$old_data/$read_data/g)
    echo "OLD: $old_line"
    echo "NEW: $var"
    sed -i s/"$old_line"/"$var"/g $2
    break
    fi
    fi
#modify the SID here.
    if [ "$1" == "-sid" ]
    then
    if [ $i -eq 2 ]
    then
    old_data="${s_array[$i]}"
    echo "CURRENT SID IS :$old_data"
    echo "ENTER NEW SID :"
    read read_data
    var=$(echo "$old_line" | sed s/$old_data/$read_data/g)
    echo "OLD: $old_line"
    echo "NEW: $var"
    sed -i s/"$old_line"/"$var"/g $2
    break
    fi
    fi

done

else
echo "null"
break
fi
IFS=$'\n'
done

