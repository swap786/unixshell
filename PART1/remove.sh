#!/bin/bash

if [ -z $1 ]
then
    echo "Usage: $0 <classlist_name>"
    exit
fi
if [ "$1" == "--help" ]
then
    echo -e "Usage: $0 <classlist_name>\n"
echo -e "<classlist_name> : Enter the class list name to be deleted.\n"
exit
fi

if [ "$1" != "" ]
then
rm -i "$1"
else
echo "Invalid Input."
fi
